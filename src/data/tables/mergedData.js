import appData from './apps.json';
import categories from './categories.json';
import developers from './developers.json';
import payment from './payment.json';
import distribution from './distribution.json'


export function getMergedData() {
    const updatedAppData = [];
    for (let i = 0; i < appData.length; i++) {
        const item = appData[i];

        updatedAppData[i] = {
            ...item,
            categoryData: categories.categories.find((el) => el.id === item.category),
            subcategoryData: categories.categories.find((el) => el.id === item.category)?.subcategories?.find((el) => el.id === item.subcategory),
            developerData: developers.find((el) => el.id === item.developerId),
            distributionData: distribution.methods.find((el) => el.id === item.distribution.method),
            paymentModelData: payment.methods.find((el) => el.id === item.paymentModel),
            routerLink: "/apps/" + item.id,
            routerLinkCategory: '/categories/' + item.category
        };
    }
    return updatedAppData;
}

export function getAppData(listOfIDs=[]) {
    const updatedAppData = [];
    let index = 0;
    for (let i = 0; i < appData.length; i++) {
        if(listOfIDs.includes(appData[i].id)) {
            const item = appData[i];
            updatedAppData[index] = {
                ...item,
                categoryData: categories.categories.find((el) => el.id === item.category),
                subcategoryData: categories.categories.find((el) => el.id === item.category)?.subcategories?.find((el) => el.id === item.subcategory),
                developerData: developers.find((el) => el.id === item.developerId),
                distributionData: distribution.methods.find((el) => el.id === item.distribution.method),
                paymentModelData: payment.methods.find((el) => el.id === item.paymentModel),
                routerLink: "/apps/" + item.id,
                routerLinkCategory: '/categories/' + item.category
            };
            index++;
        }
    }
    return updatedAppData;
}
export function getDeveloperData(developerId) {
    return developers.filter(item => item.id === developerId)[0]
}
export function getAppsByDeveloper(developerId) {
    const updatedAppData = [];
    for (let i = 0; i < appData.length; i++) {
        const item = appData[i];
        updatedAppData[i] = {
            ...item,
            categoryData: categories.categories.find((el) => el.id === item.category),
            subcategoryData: categories.categories.find((el) => el.id === item.category)?.subcategories?.find((el) => el.id === item.subcategory),
            developerData: developers.find((el) => el.id === item.developerId),
            distributionData: distribution.methods.find((el) => el.id === item.distribution.method),
            paymentModelData: payment.methods.find((el) => el.id === item.paymentModel),
            routerLink: "/apps/" + item.id,
            routerLinkCategory: '/categories/' + item.category
        };
    }
    return updatedAppData.filter((el) => el.developerId == developerId)
}



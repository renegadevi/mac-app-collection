/* ------------------------------------------------------------------------
 * Core imports
 * ------------------------------------------------------------------------ */

// import vue and router
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import jsConsoleFormatter from 'js-console-formatter';


/* ------------------------------------------------------------------------
 * UI Library imports
 * ------------------------------------------------------------------------ */
import "@/assets/sass/main.scss"
import 'bootstrap-icons/font/bootstrap-icons.css'

/* ------------------------------------------------------------------------
 * Initialization process
 * ------------------------------------------------------------------------ */

// Enable console-formatter in development.
if (process.env.NODE_ENV === 'development') {
   jsConsoleFormatter()
   console.debug("jsConsoleFormatter: initialized")
}

/* --------------------------------------------------------------------------
 * Create app -process
 * -------------------------------------------------------------------------- */

// Create vue app
const app = createApp(App);

// Add internal router to prevent website refreashes
app.use(router);

// Push the app to show in our html
app.mount("#app");
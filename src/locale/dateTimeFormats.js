const datetimeFormats = {
    'en': {
        datetime: {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            hour12: true
        },
        date: {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        },
        time: {
            hour: 'numeric',
            minute: 'numeric',
            hour12: true
        },
        short: {
            year: 'numeric',
            month: 'short',
            day: 'numeric'
        },
        long: {
            year: 'numeric',
            month: 'short',
            day: 'numeric',
            weekday: 'short',
            hour: 'numeric',
            minute: 'numeric',
            hour12: true
        }
    }
}
export default datetimeFormats;
import { createRouter, createWebHistory } from "vue-router";


// get list of categories, developers and tags
import categories from "@/data/tables/categories.json";
import developers from "@/data/tables/developers.json";
import apps from "@/data/tables/apps.json"


const categoryList = [...categories.categories.map(category => category.id)];
const tagList = [...new Set(apps.map(item => {
  const updatedTags = item.tags.map(tag => tag.replace(/\s+/g, '-')); // Replace all spaces with dashes
  return {...item, tags: updatedTags};
}).map(item => item.tags).flat())]
const developerList = [...new Set(developers.map(el => el.id))]
const appIds = [...apps.map(item => item.id)]


// routes
const routes = [
  {
    path: '/',
    name: 'root',
    redirect: { name: 'home' },
    meta: {title:''},
    children: [
      {
        path: '/',
        name: 'home',
        component: () => import("@/views/HomePage.vue"),
        meta: {title:'The Best Place To Find Mac Apps'},
      },
      {
        path: '/apps',
        name: 'apps',
        component: () => import("@/views/ShowAllAppsPage.vue"),
        meta: {title:'Apps'},
      },
      {
        path: '/apps/:appSlug([a-z0-9-]*$)',
        name: 'app',
        component: () => import("@/views/AppPage.vue"),
        beforeEnter(to) {
          if (!appIds.includes(to.params.appSlug)) {
            return {
              name: 'not-found',
              // Match the path of your current page and keep the same url...
              params: { pathMatch: to.path.split('/').slice(1) },
              query: to.query,
              hash: to.hash,
            }
          }
        }
      },
      {
        path: '/categories',
        name: 'categories',
        component: () => import("@/views/CategoriesPage.vue"),
        meta: {title:'Categories'},
      },
      {
        path: '/developers',
        name: 'developers',
        component: () => import("@/views/DevelopersPage.vue"),
        meta: {title:'Developers'},
      },
      {
        path: '/recently-added',
        name: 'recently-added',
        component: () => import("@/views/RecentAppsPage.vue"),
        meta: {title:'Recently Added'},
      },
      {
        path: '/categories/:categoryName([a-z0-9-]*$)',
        name: 'category',
        //sensitive: true,
        component: () => import("@/views/CategoryPage.vue"),
        meta: {title:'Category'},
        beforeEnter(to) {
          if (!categoryList.includes(to.params.categoryName)) {
            return {
              name: 'not-found',
              // Match the path of your current page and keep the same url...
              params: { pathMatch: to.path.split('/').slice(1) },
              query: to.query,
              hash: to.hash,
            }
          }
        },

      },
      {
        path: '/tag/:tagName([a-z0-9-]*$)',
        name: 'tag',
        component: () => import("@/views/TagPage.vue"),
        meta: {title:'Tag'},
        beforeEnter(to) {
          if (!tagList.includes(to.params.tagName)) {
            return {
              name: 'not-found',
              // Match the path of your current page and keep the same url...
              params: { pathMatch: to.path.split('/').slice(1) },
              query: to.query,
              hash: to.hash,
            }
          }
        }
      },
      {
        path: '/developer/:developerName([a-z0-9-]*$)',
        name: 'developer',
        component: () => import("@/views/DeveloperPage.vue"),
        meta: {title:'Developer'},
        beforeEnter(to) {

          if (!developerList.includes(to.params.developerName)) {
            return {
              name: 'not-found',
              // Match the path of your current page and keep the same url...
              params: { pathMatch: to.path.split('/').slice(1) },
              query: to.query,
              hash: to.hash,
            }
          }
        }
      },
      {
        path: '/curation',
        name: 'curation',
        meta: {title:'Curation'},
        component: () => import("@/views/SoftwareCurationPage.vue"),
      },
      {
        path: '/ultimate-picks',
        name: 'ultimate-picks',
        meta: {title:'Ultimate Picks'},
        component: () => import("@/views/UltimatePicksPage.vue"),
      }
    ]
  },
  {
    path: "/:pathMatch(.*)*",
    name: "not-found",
    component: () => import('@/views/error/ErrorPage404.vue'),
    meta: {title:'404 Page not found'},
  },
  {
    path: "/:pathMatch(.*)",
    name: "bad-not-found",
    meta: {title:'404 Page not found'},
    component: () => import('@/views/error/ErrorPage404.vue')
  }
];

// Create router and assign history and routes.
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior (to) {
    if (to.hash) {
      return {
        el: to.hash,
        left: 0,
        top: 100
      }
    }
  },
});


router.beforeEach(async (to) => {

  // Scroll to top, and not re-route in the middle of the page
  window.scrollTo(0, 0);

  // Update <title></title>
  document.title = to.meta.title
    ? to.meta.title + " | Mac App Collection"
    : "Mac App Collection";

});

export default router;

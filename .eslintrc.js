module.exports = {
    env: {
        node: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:vue/vue3-recommended'
    ],
    rules: {
        // 'vue/script-setup-uses-vars': 'error',
        //'no-unused-vars': 'off',
        //'vue/script-setup-uses-vars': 'error',
        // "no-undef": "off",
        // "vue/no-unused-vars": "off",
        // "no-dupe-else-if": "off",
        //"vue/no-reserved-component-names": "off",
        //'vue/multi-word-component-names': "off",
        //"vue/require-v-for-key": "off",
        // "vue/no-use-v-if-with-v-for": "off",
        // "vue/no-v-html": "off",
        // "vue/no-duplicate-attributes": "off",
        // "vue/require-default-prop": "off",
        // "vue/require-explicit-emits": "off",
        // "vue/v-on-event-hyphenation": "off",
        // "vue/no-lone-template": "off",
        // "vue/no-template-key": "off",
        // "vue/no-useless-template-attributes": "off",
        // "vue/no-template-shadow": "off"
    }
}
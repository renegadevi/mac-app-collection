import { fileURLToPath } from "url";
import { defineConfig } from "vite";
import mkcert from 'vite-plugin-mkcert'
import { resolve, dirname } from 'path';
import vue from "@vitejs/plugin-vue";
import eslintPlugin from 'vite-plugin-eslint';

const __metaURL = import.meta.url;
const __filename = fileURLToPath(__metaURL);
const __dirname = dirname(__filename);


// https://vitejs.dev/config/
export default defineConfig({
  base: `${process.env.BASE_PATH || '/'}`,
  server: { https: true },
  plugins: [
    mkcert(),
    eslintPlugin(),
    vue()
  ],
  test: {
    globals: true
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
    }
  },
});
